{-# LANGUAGE PatternSynonyms, ScopedTypeVariables #-}
module Reference.BinaryTree
  ( BinaryTree(..)
  , pattern Final, pattern Left, pattern Right
  , height
  , children, root
  , tree0, tree1, tree2, tree3, tree4) where
import Prelude hiding (Left, Right)
import Data.Maybe (maybeToList)

data BinaryTree a = Leaf | Branch a (BinaryTree a) (BinaryTree a) deriving (Functor, Foldable)
pattern Final a = Branch a Leaf Leaf
pattern Left a left = Branch a left Leaf
pattern Right a right = Branch a Leaf right

zipWithDefault :: a -> (a -> a -> b) -> [a] -> [a] -> [b]
zipWithDefault x f as [] = flip f x <$> as
zipWithDefault x f [] bs = f x <$> bs
zipWithDefault x f (a:as) (b:bs) = f a b : zipWithDefault x f as bs

instance Show a => Show (BinaryTree a) where
  show Leaf = "Leaf"
  show t = mconcat $ map (++ "\n") $ lines t
    where
      lines :: BinaryTree a -> [String]
      lines Leaf = [""]
      lines (Final a) = [show a]
      lines (Branch x l r) =
        let left = lines l
            right  = lines r
            element = show x
            elemEmpty = spaces (length element)
            childLen = max (maximum $ length <$> left) (maximum $ length <$> right)
            totalLen = childLen * 2 + length element
            childLines = zipWithDefault "" (\l r -> expand childLen l ++ elemEmpty ++ expand childLen r) left right
            elemLine = expand totalLen element
            connectingLines = fmap (expand totalLen) $
              let shift = childLen `div` 2
              in if shift <= 2
               then ["/" ++ elemEmpty ++ "\\"]
               else [ replicate (shift - 2) '_' ++ "/" ++ elemEmpty ++ "\\" ++ replicate (shift - 2) '_'
                    , "/" ++ spaces (shift - 1) ++ elemEmpty ++ spaces (shift - 1) ++ "\\"
                    ]
        in (elemLine:connectingLines) ++ childLines
      spaces :: Int -> String
      spaces = flip replicate ' '
      expand :: Int -> String -> String
      expand n str | extra <= 0 = str
                   | otherwise  = spaces left ++ str ++ spaces right
        where
          extra = n - length str
          right = extra `div` 2
          left = extra - right

br :: a -> BinaryTree a -> BinaryTree a -> BinaryTree a
lf :: BinaryTree a
br = Branch
lf = Leaf

tree0 :: BinaryTree Int
tree1 :: BinaryTree Int
tree2 :: BinaryTree Int
tree3 :: BinaryTree Int
tree4 :: BinaryTree Int
tree0 = Branch 5 (Branch 2 Leaf Leaf) Leaf
tree1 = Branch 0 (Branch 4 (Branch 2 Leaf Leaf) (Branch 3 Leaf Leaf))
          (Branch 10 Leaf (Branch 12 Leaf Leaf))
tree2 = Branch 5 (Branch 4 (Branch 2 Leaf Leaf) (Branch 3 Leaf Leaf))
          (Branch 10 Leaf (Branch 12 Leaf Leaf))
tree3 = Branch 5 (Branch 4 (Branch 2 Leaf (Branch 3 Leaf Leaf)) Leaf)
          (Branch 10 Leaf (Branch 12 Leaf Leaf))
tree4 = br 5 (br 3 (br 1 lf lf) lf) (br 9 (br 8 lf lf) (br 10 lf (br 12 lf lf)))

search :: BinaryTree Int -> Int -> Bool
search Leaf _ = False
search (Branch x left right) y = x == y || search left y || search right y

inorder :: BinaryTree a -> [a]
inorder Leaf = []
inorder (Branch x left right) = inorder left ++ x:inorder right

removeRoot :: BinaryTree Int -> Maybe (BinaryTree Int)
removeRoot Leaf = Nothing
removeRoot (Final x) = Just Leaf
removeRoot (Branch _ l r) = case largest l of
   Nothing      -> Just r
   Just (x, l') -> Just (Branch x l' r)

largest :: BinaryTree Int -> Maybe (Int, BinaryTree Int)
largest Leaf = Nothing
largest (Branch x l r) = case largest r of
  Nothing -> Just (x, l)
  Just (y, r') -> Just (y, Branch x l r')

height :: BinaryTree a -> Int
height Leaf = 0
height (Branch _ l r) = 1 + max (height l) (height r)

root :: BinaryTree a -> Maybe a
root Leaf = Nothing
root (Branch a _ _) = Just a

children :: BinaryTree a -> [a]
children Leaf = []
children (Branch _ l r) = maybeToList (root l) ++ maybeToList (root r)