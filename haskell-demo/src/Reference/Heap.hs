{-# LANGUAGE GADTs, DataKinds, PatternSynonyms #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Reference.Heap
  ( LeftistHeap(Leaf), pattern LeftistNode, makeLeftistNode, meld
  ) where
import qualified BinaryTree as T
import BinaryTree (BinaryTree)

type Heap a = BinaryTree a

-- for binary heaps
almostFull :: BinaryTree a -> Bool
almostFull tree = maximum leafLevels - minimum leafLevels <= 1
  where
    getLevels :: BinaryTree a -> [Int]
    getLevels T.Leaf = [0]
    getLevels (T.Branch _ l r) = map (+1) (getLevels l ++ getLevels r)
    leafLevels :: [Int]
    leafLevels = getLevels tree

heapProperty :: Ord a => BinaryTree a -> Bool
heapProperty T.Leaf = True
heapProperty tree@(T.Branch element left right) =
  all (element <=) (T.children tree) && heapProperty left && heapProperty right

isBinaryHeap :: Ord a => BinaryTree a -> Bool
isBinaryHeap tree = almostFull tree && heapProperty tree

-- for leftist heaps
data LeftistHeap a = Leaf | Node Int a (LeftistHeap a) (LeftistHeap a)
pattern LeftistNode k l r <- Node _ k l r

toHeap :: LeftistHeap a -> Heap a
toHeap Leaf = T.Leaf
toHeap (Node _ x l r) = T.Branch x (toHeap l) (toHeap r)

rank :: LeftistHeap a -> Int
rank Leaf = 0
rank (Node r _ _ _) = r

makeLeftistNode :: a -> LeftistHeap a -> LeftistHeap a -> LeftistHeap a
makeLeftistNode x l r
  | rl < rr   = Node (rl + 1) x r l
  | otherwise = Node (rr + 1) x l r
  where (rl, rr) = (rank l, rank r)

meld :: Ord a => LeftistHeap a -> LeftistHeap a -> LeftistHeap a
meld l Leaf = l
meld Leaf r = r
meld l@(Node _ k0 l0 r0) r@(Node _ k1 l1 r1)
  | k0 < k1   = makeLeftistNode k0 l0 $ meld r0 r
  | otherwise = makeLeftistNode k1 l1 $ meld l r1

filter :: (a -> Bool) -> LeftistHeap a -> LeftistHeap a
filter p Leaf = Leaf
filter p (LeftistNode x l r)
  | p x       = meld (filter p l) (filter p r)
  | otherwise = case delete p l of
    Nothing -> makeLeftistNode x l <$> delete p r
    Just l1 -> Just $ makeLeftistNode x l1 r

{-
data LeftistHeap a (n :: Nat) where
  Leaf :: LeftistHeap a 0
  Node :: (KnownNat n, n <= m) => a -> LeftistHeap a m -> LeftistHeap a n -> LeftistHeap a (m + 1)

toHeap :: LeftistHeap a n -> Heap a
toHeap Leaf = T.Leaf
toHeap (Node x l r) = T.Branch x (toHeap l) (toHeap r)

instance Show a => Show (LeftistHeap a n) where
  show = show . toHeap

rank :: KnownNat n => LeftistHeap a n -> Int
rank = fromInteger . natVal

makeLeftistNode :: (KnownNat m, KnownNat n) => a -> LeftistHeap a m -> LeftistHeap a n -> LeftistHeap a (Max m n + 1)
makeLeftistNode x h1 h2 = case cmpNat h1 h2 of
  LTI -> Node x h2 h1
  EQI -> Node x h1 h2
  GTI -> case cmpNat h2 h1 of
    LTI -> Node x h1 h2

max0n :: KnownNat n => proxy n -> Max 0 n :~: n
max0n p = case cmpNat Leaf p of
  LTI -> Refl
  EQI -> Refl

maxn0 :: KnownNat n => proxy n -> Max n 0 :~: n
maxn0 p = case cmpNat p Leaf of
  EQI -> Refl
  GTI -> Refl

meld :: (KnownNat x, KnownNat y) => LeftistHeap a x -> LeftistHeap a y -> LeftistHeap a (Max x y)
meld Leaf    r = case maxn0 r of Refl -> r
meld    l Leaf = case max0n l of Refl -> l
meld l@(Node x1 l1 r1) r@(Node x2 l2 r2)
  | x1 <= x2  = makeLeftistNode x1 l1 $ meld r1 r
  | otherwise = makeLeftistNode x2 l2 $ meld r2 l

FAILURE: Agda needed
-}