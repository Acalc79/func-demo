{-# LANGUAGE PatternSynonyms, ScopedTypeVariables #-}
module Reference.Initial where
import Prelude as P (Show(..), Int, String, div, (==), (<), (<=), (>), (++))
import Debug.Trace (traceShow)

data Bool = True | False deriving Show
data LimitedInt = Zero | One | Two | Three

data Optional a = Nothing | Is a deriving Show
division :: (Int, Int) -> Optional Int
division (n, k) = if k == 0 then Nothing else Is (div n k)

data List a = Empty | a :. (List a)

instance Show a => Show (List a) where
  show list = "[" ++ helper list ++ "]"
    where helper :: List a -> String
          helper Empty = ""
          helper (Single x) = show x
          helper (x :. rest) = show x ++ ", " ++ helper rest

infixr 5 :.
-- requires: PatternSynonyms
pattern Single x = x :. Empty

numbers :: List Int
numbers = 0 :. 1 :. -2 :. Single 3

minList :: List Int -> Int
minList Empty = 0
minList (Single x) = x
minList (x :. rest) = if minList rest < x then minList rest else x

withDefault :: Optional a -> a -> a
withDefault Nothing def = def
withDefault (Is x) def = x

min :: Int -> Int -> Int
min a b = if a > b then b else a

minList' :: List Int -> Optional Int
minList' Empty = Nothing
minList' (x :. rest) = let y = withDefault (minList' rest) x in
  Is (min y x)

-- add element to the end of the list
append :: List a -> a -> List a
append Empty element = Single element
append (x :. rest) element = x :. append rest element

reverse :: List Int -> List Int
reverse Empty = Empty
reverse (x :. rest) = append (reverse rest) x

swap :: List a -> List a
swap (x :. y :. rest) = y :. x :. rest
swap list = list

isSorted :: List Int -> Bool
isSorted Empty = True
isSorted (Single x) = True
isSorted (x :. rest@(y :. _)) = if x <= y then isSorted rest else False

-- pass through the list once, doing the bubbling
singleBubble :: List Int -> List Int
singleBubble Empty = Empty
singleBubble x@(Single _) = x
singleBubble (x :. y :. rest) = if x < y
  then x :. singleBubble (y :. rest)
  else y :. singleBubble (x :. rest)

bubbleSort :: List Int -> List Int
bubbleSort list = case isSorted list of
   True -> list
   False -> traceShow list (bubbleSort (singleBubble list))