{-# LANGUAGE ScopedTypeVariables #-}
module Reference.Graph where
import Data.List (partition, nub)
import Data.Maybe (isJust, listToMaybe, catMaybes, mapMaybe)

data Graph v e = Graph [v] [[Maybe e]] deriving (Show, Eq, Ord)
type Path = [Int]

replace :: Graph v e -> Int -> Int -> Maybe e -> Graph v e
replace (Graph v e) i j new = Graph v $ modifyAt i (modifyAt j (const new)) e
  where
   modifyAt :: Int -> (a -> a) -> [a] -> [a]
   modifyAt _ _     [] = []
   modifyAt 0 f (x:xs) = f x:xs
   modifyAt n f (x:xs) = x : modifyAt (n-1) f xs

isUndirected :: Eq e => Graph v e -> Bool
isUndirected (Graph vs es) = and $ do
  i <- [0..length vs]
  j <- [0..i-1]
  return $ es !! i !! j == es !! j !! i

neighbours :: Graph v e -> Int -> [Int]
neighbours (Graph _ es) i = catMaybes $ zipWith (<$) [0..] (es !! i)

isPath :: Graph v e -> Path -> Bool
isPath            _  [] = False
isPath            _ [_] = False
isPath (Graph _ es)   p = and $ zipWith (\i j -> isJust $ es !! i !! j) p (tail p)

pathLen :: Path -> Int
pathLen p = length p - 1

isCycle :: Graph v e -> Path -> Bool
isCycle g p = isPath g p && last p == head p && nub (tail p) == tail p

type PathMatrix = [[Maybe Path]]

findCycle :: Graph v e -> Maybe Path
findCycle g@(Graph vdata es) = listToMaybe $ mapMaybe findCycle $ map reachable vs
  where
    vs = [0..length vdata - 1]
    edgeMatrix :: PathMatrix
    edgeMatrix = zipWith (\i e -> [i] <$ e) [0..] <$> es
    reachable :: Int -> PathMatrix
    reachable 0 = edgeMatrix
    reachable n = let r = reachable (n - 1)
      in [[
        listToMaybe $ catMaybes [(++) <$> r !! i !! k <*> edgeMatrix !! k !! j | k <- vs]
        | j <- vs] | i <- vs]
    findCycle :: PathMatrix -> Maybe Path
    findCycle matrix = listToMaybe $ catMaybes $ (\i -> (i:) <$> matrix !! i !! i) <$> vs

dfs :: Graph v e -> Int -> [Int]
dfs g x = helper [x] []
  where
    helper :: [Int] -> [Int] -> [Int]
    helper []     visited = reverse visited
    helper (x:xs) visited = helper (newVertices ++ xs) $ x:visited
      where newVertices = filter (\n -> not $ n `elem` visited) $ neighbours g x

o = Nothing
l = Just ()
-- examples
graph0 = Graph [0..4]
  [ [o, l, o, l, o]
  , [o, o, l, l, l]
  , [l, o, o, o, o]
  , [l, o, l, o, o]
  , [o, o, o, o, o]
  ]

dfs :: Graph v e -> Int -> [Int]
dfs g x = helper [x] []
  where
    helper :: [Int] -> [Int] -> [Int]
    helper []     visited = reverse visited
    helper (x:xs) visited = helper (newVertices ++ xs) $ x:visited
      where newVertices = filter (\n -> not $ n `elem` visited) $ neighbours g x