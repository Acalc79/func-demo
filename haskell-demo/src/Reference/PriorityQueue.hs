{-# LANGUAGE PatternSynonyms, ScopedTypeVariables, FunctionalDependencies #-}
module Reference.PriorityQueue where
import BinaryTree (BinaryTree(..), pattern Final)
import Heap as H (LeftistHeap(..), meld)

class PriorityQueue c k a | c -> k, c -> a where
  empty :: c
  insert :: k -> a -> c -> c
  extractMin :: c -> (Maybe a, c)
  decreaseKey :: k -> a -> c -> c
  delete :: a -> c -> c

  first :: c -> Maybe a
  first = fst . extractMin

insertAll :: PriorityQueue c k a => [(k, a)] -> c -> c
insertAll []       = id
insertAll ((k, x) : ps) = insertAll ps . insert k x

instance (Ord k, Eq a) => PriorityQueue [(k,a)] k a where
  empty = []

  insert k x [] = [(k, x)]
  insert k x ls1@((k1, x1) : xs)
    | k < k1    = ( k,  x) : ls1
    | otherwise = (k1, x1) : insert k x xs

  extractMin [] = (Nothing, [])
  extractMin ((_, x):xs) = (Just x, xs)

  decreaseKey k x [] = []
  decreaseKey k x ((k1, x1) : ls)
    | x1 == x        = ( k, x1) : ls
    | k >= k1        = (k1, x1) : decreaseKey k x ls
    | otherwise      = case remove x ls of
        Nothing        -> (k1, x1) : ls
        Just (x2, ls2) -> ( k, x2) : (k1, x1) : ls2
    where
      remove x [] = Nothing
      remove x ((k1, x1) : ls)
        | x == x1   = Just (x1, ls)
        | otherwise = case remove x ls of
          Nothing        -> Nothing
          Just (x2, ls2) -> Just (x2, (k1, x1) : ls2)

  delete x [] = []
  delete x (i@(_, x1) : ls)
    | x == x1   = ls
    | otherwise = i : delete x ls

instance (Ord k, Eq a) => PriorityQueue (LeftistHeap (k, a)) k a where
  empty = H.Leaf

  insert k x = meld (makeLeftistNode ) :: k -> a -> c -> c

  extractMin Leaf = Nothing
  extractMin (LeftistNode x l r) = Just (x, meld l r)

  decreaseKey k x Leaf = Leaf
  decreaseKey k x (LeftistNode x l r) = undefined

  delete x Leaf = Leaf
  delete x (LeftistNode y l r) =

test :: PriorityQueue c Int Char => c
test = insertAll [(1, 'a'), (2, 'c'), (0, 'b'), (6, 'f')] $ empty