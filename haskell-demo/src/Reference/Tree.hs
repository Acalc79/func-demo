module Reference.Tree (Tree(..), binomialOrder) where

data Tree a = Tree a [Tree a] deriving (Show, Functor, Foldable)

binomialOrder :: Tree a -> Maybe Int
binomialOrder (Tree _ []) = Just 0
binomialOrder (Tree x (t:ts)) = do
  order1 <- binomialOrder (Tree x ts)
  order2 <- binomialOrder ts
  if order1 == order2 then return (order1 + 1) else Nothing