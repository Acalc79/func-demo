{-# LANGUAGE ScopedTypeVariables, FunctionalDependencies #-}
module Graphs where

import qualified Data.Map as Map
import Data.Maybe (fromJust, fromMaybe)
import Debug.Trace (trace)

-- class Eq a where
--  (==) :: a -> a -> Bool

-- abstract data type declaration
class Graph g v e | g -> v, g -> e where
  vertices :: g -> [v]
  edges :: g -> [e]
  -- tells us whether there is an edge from the first v to the second one
  getEdge :: g -> v -> v -> Maybe e
  -- gives all neighbours of a vertex
  neighbours :: g -> v -> [v]
  fromAdjacencyList :: ([v], [[e]]) -> g

type V = Int
type Edge = (V, V)
instance Graph [[Edge]] V Edge where
  vertices list = [0..length list - 1]

  edges list = concat list
  -- equivalent to: edges = concat

  getEdge list v0 v1 = if (v0, v1) `elem` (list !! v0)
    then Just (v0, v1)
    else Nothing
  -- !! - take the element at index

  neighbours list v = map snd (list !! v)
  -- list !! 1 is e.g. [(1, 3), (1, 5), (1, 6)]

  fromAdjacencyList (vs, edgeList) = fromMaybe [] . (`lookup` zip vs edgeList) <$> [0..maximum vs]

-- main graph issue of interest: finding a path from some start vertex
--  to a goal vertex

-- two simple approaches:
--  * depth-first search - go to neighbours as soon as you find one,
--      continue from that neighbour the same way until dead end reached,
--      at which point we go back one level
--  * breadth-first search - go through all the neighbours, only then
--      through all their neighbours in the same order and so on
-- BOTH: need to keep track of which vertices we've already visited (not a tree)

-- arguments: graph, start vertex, goal vertex
-- returns: Maybe [e] representing the path from start to goal, if exists
-- dfs :: Graph g v e => g -> v -> v -> Maybe [e]
-- attempt no. 1: try to simply follow the picture
{-dfs graph s g = searchFrom s [s]
  where searchFrom :: v -> [v] -> Maybe [e]
        searchFrom x visited =
          let ns = neighbours g x
              newNs = filter (\y -> not (y `elem` visited)) ns
          in case newNs of
            []     -> Nothing
            (y:ys) -> let path = searchFrom y (y : visited)
                      in case path of
                        Nothing -> -- look at the next vertex in ys
                        Just es -> -- add edge x -> y to 'es' and return that
            -- the built-in lists are defined as:  [] | x:xs
-}

-- attempt no. 2: use a stack to remember the leftover vertices we can go back to
dfsIsTherePath :: forall g v e. (Graph g v e, Eq v) => g -> v -> v -> Bool
dfsIsTherePath graph s goal = searchFromWithStack [] [s]
  where searchFromWithStack :: [v] -> [v] -> Bool
        searchFromWithStack visited []                  = False
        searchFromWithStack visited (x : leftoverStack) =
          if x == goal
            then True
          else if x `elem` visited
            then searchFromWithStack visited leftoverStack
              -- if 'x' was visited before and we are still looking, there was no path from it
            else searchFromWithStack (x : visited) (neighbours graph x ++ leftoverStack)
-- searchFromWithStack X S with context (s, goal)
-- induction on <, where (X₁, S₁) > (X₂, S₂) precisely when
-- (|S₁| ≥ 1 ∧ x = head S₁ ∧ x ≠ goal ∧ ((x ∈ X₁ ∧ X₂ = X₁ ∧ S₂ = tail S₁) ∨ (x ∉ X₁ ∧ X₂ = x : X₁ ∧ S₂ = [v ∈ V : (x v) ∈ E] ∪ tail S₁)))
-- well founded by well-foundedness of lexicographic (|V| - |X|, |S|)
-- claim:
-- s ∈ X ∪ S
-- ∧ (∀ v ∈ X ∪ S. ∃ path. path : s → v)
-- ∧ (∀ v ∈ V. ∀ path : s → v. v ∈ X ∨ s ∈ S ∨ ∃ w ∈ X, u ∈ S. (w, u) ∈ path)
-- ∧ goal ∉ X


-- attempt no. 2: use a stack to remember the leftover vertices we can go back to
-- dfsFindPath :: forall g v e. (Show g, Show v, Show e, Ord v, Graph g v e) => g -> v -> v -> Maybe [e]
dfsFindPath :: forall g v e. (Ord v, Graph g v e) => g -> v -> v -> Maybe [e]
dfsFindPath graph s goal = searchFromWithStack [] (Map.singleton s []) [s]
  where searchFromWithStack :: [v] -> Map.Map v [e] -> [v] -> Maybe [e]
--        searchFromWithStack visited pathsSoFar stack | trace (show visited ++ show stack ++ show pathsSoFar) False = undefined
        searchFromWithStack visited pathsSoFar []                  = Nothing
        searchFromWithStack visited pathsSoFar (x : leftoverStack) =
          if x == goal
            then pathsSoFar Map.!? x -- we don't expect this to give Nothing, but just in case
          else if x `elem` visited
            then searchFromWithStack visited pathsSoFar leftoverStack
              -- if 'x' was visited before and we are still looking, there was no path from it
            else let ns = neighbours graph x
                     pathForX = pathsSoFar Map.! x
                     addLastEdge n = (n, pathForX ++ [fromJust (getEdge graph x n)])
                     pathsFromX = Map.fromList (map addLastEdge ns)
                     newPaths = Map.union pathsSoFar pathsFromX
              in searchFromWithStack (x : visited) newPaths (ns ++ leftoverStack)

dfsFindPath2 :: forall g v e. (Ord v, Graph g v e) => g -> v -> v -> Maybe [e]
dfsFindPath2 graph s goal = searchWithStack [] [(s, [])]
  where searchWithStack :: [v] -> [(v, [e])] -> Maybe [e]
        searchWithStack visited []                              = Nothing
        searchWithStack visited ((x, pathForX) : leftoverStack) =
          if x == goal
            then Just pathForX
          else if x `elem` visited
            then searchWithStack visited leftoverStack
            else let ns = neighbours graph x
                     -- idea: have pathForX : s -> ... -> x
                     -- add to it the edge: x -> n
                     addLastEdge n = (n, pathForX ++ [fromJust (getEdge graph x n)])
                     newVerticesWithPaths = map addLastEdge ns
              in searchWithStack (x : visited) (newVerticesWithPaths ++ leftoverStack)

bfsFindPath2 :: forall g v e. (Ord v, Graph g v e) => g -> v -> v -> Maybe [e]
bfsFindPath2 graph s goal = searchWithQueue [] [(s, [])]
  where searchWithQueue :: [v] -> [(v, [e])] -> Maybe [e]
        searchWithQueue visited []                              = Nothing
        searchWithQueue visited ((x, pathForX) : leftoverQueue) =
          if x == goal
            then Just pathForX
          else if x `elem` visited
            then searchWithQueue visited leftoverQueue
            else let ns = neighbours graph x
                     -- idea: have pathForX : s -> ... -> x
                     -- add to it the edge: x -> n
                     addLastEdge n = (n, pathForX ++ [fromJust (getEdge graph x n)])
                     newVerticesWithPaths = map addLastEdge ns
              in searchWithQueue (x : visited) (leftoverQueue ++ newVerticesWithPaths)