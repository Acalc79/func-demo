module DataStructures where

data BinaryTree a = Leaf | Branch a (BinaryTree a) (BinaryTree a)
  deriving Show

inorder :: BinaryTree a -> [a]
inorder Leaf = []
inorder (Branch element left right) = inorder left ++ [element] ++ inorder right

tree0 :: BinaryTree Int
tree1 :: BinaryTree Int
tree2 :: BinaryTree Int
tree3 :: BinaryTree Int
tree0 = Branch 5 (Branch 2 Leaf Leaf) Leaf
tree1 = Branch 0 (Branch 4 (Branch 2 Leaf Leaf) (Branch 3 Leaf Leaf))
          (Branch 10 Leaf (Branch 12 Leaf Leaf))
tree2 = Branch 5 (Branch 4 (Branch 2 Leaf Leaf) (Branch 3 Leaf Leaf))
          (Branch 10 Leaf (Branch 12 Leaf Leaf))
tree3 = Branch 5 (Branch 4 (Branch 2 Leaf (Branch 3 Leaf Leaf)) Leaf)
          (Branch 10 Leaf (Branch 12 Leaf Leaf))

isSearchTree :: Ord a => BinaryTree a -> Bool
isSearchTree Leaf = True
isSearchTree (Branch element left right) =
  all (< element) (inorder left) && all (> element) (inorder right)
  && isSearchTree left && isSearchTree right
-- (< element) is a shorthand for \x -> x < element

-- goal: check if a condition holds for all elements of a list
-- type: [a] -> (a -> Bool) -> Bool
-- found: all :: (a -> Bool) -> [a] -> Bool

search :: Ord a => a -> BinaryTree a -> Bool
search x Leaf = False
search x (Branch element left right) = if x == element
  then True
  else search x (if x < element then left else right)

-- search :: (Int, BinaryTree Int) -> Bool
-- (a, b) - a pair of elements, analogous to cartesian product
--   technically is a single (more complex) type
-- a -> (b -> c)

-- in Java, if I want to have two functions, one of which takes two arguments,
-- the other taking just one of them with some value for the other
-- I would need to use ...
-- e.g. have translate(Language input, Language output)
--   and want translate(Language input) which always translates to English
--   this is polymorphism, where
--     translate(Language input) {
--       return translate(input, new English();
--     }

-- remove root from a search tree
removeRoot :: BinaryTree a -> BinaryTree a
removeRoot Leaf = Leaf
removeRoot (Branch x Leaf Leaf) = Leaf
removeRoot (Branch x left Leaf) = left
removeRoot (Branch x Leaf right) = right
removeRoot (Branch x left right) = let
    (maxFromLeft, left1) = extractMaximum left
  in Branch maxFromLeft left1 right

-- assumes the tree is not a leaf
extractMaximum :: BinaryTree a -> (a, BinaryTree a)
extractMaximum (Branch x left Leaf) = (x, left)
extractMaximum (Branch x left right) = let
    (maxFound, right1) = extractMaximum right
  in (maxFound, Branch x left right1)

-- corresponding concept to OOP
-- in Haskell called "type classes"
-- e.g. "Eq a" for types "a" that can be compared for equality
--   and "Ord a" if they can be compared
--   "Show a" means "a" can be converted to string (like toString() in Java)