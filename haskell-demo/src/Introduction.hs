{-# LANGUAGE NoImplicitPrelude #-}
module Introduction where
import Prelude (Show, Int, div, (==), (<), (>))

-- we can define our own basic data types
-- called ADT, algebraic data type, (not "abstract" in this context)

data Bool = True | False deriving (Show)

x :: Bool
x = True

-- we can, for example, limit integers to a couple of values
-- checked statically at compile time
data LimitedInt = Zero | One | Two | Three

-- we can include type parameters
data Optional a = Nothing | Is a deriving (Show)

division :: (Int, Int) -> Optional Int
division (n, k) = if k == 0 then Nothing else Is (div n k)

-- let's create data structures
data List a = Empty | NonEmpty a (List a) deriving (Show)

-- Java operators: >=, ==, <=, +, -, /, _?_:_
-- in Haskell we can define our own operators
-- 'r' stands for "associate to the right", e.g. put brackets on the rhs when in doubt
infixr 5 >:
(>:) :: a -> List a -> List a
(>:) = NonEmpty

-- list of 0, 1, 2, 3
numbers :: List Int
numbers = NonEmpty 0 (NonEmpty 1 (NonEmpty (-2) (NonEmpty 3 Empty)))
numbers' = 0 >: 1 >: -2 >: 3 >: Empty

-- task: find minimum of a list
-- in Java: set minimum to first element, go through the list comparing to the minimum
--    update minimum when smaller value is found
-- in functional: no updating values, no loops
minList :: List Int -> Int
-- new concept in functional programming: pattern matching

minList Empty = 0
minList (NonEmpty x Empty) = x
minList (NonEmpty x rest) = if minList rest < x then minList rest else x
   -- min rest - minimum of the elements after the first one
   -- e.g. for "min numbers", we have "min (NonEmpty 0 [1, -2, 3])"
   --          then "min rest" is "min [1, -2, 3]", so -2

withDefault :: Optional a -> a -> a
withDefault Nothing def = def
withDefault (Is x) def = x

min :: Int -> Int -> Int
min a b = if a > b then b else a

minList' :: List Int -> Optional Int
minList' Empty = Nothing
minList' (NonEmpty x rest) = let y = withDefault (minList' rest) x in
  Is (min y x)

-- add element to the end of the list
append :: List a -> a -> List a
append Empty element = NonEmpty element Empty
append (NonEmpty x rest) element = NonEmpty x (append rest element)

reverse :: List Int -> List Int
reverse Empty = Empty
reverse (NonEmpty x rest) = append (reverse rest) x
 -- somehow put rest first and x at the end

swap :: List a -> List a
swap (NonEmpty x (NonEmpty y rest)) = y >: x >: rest
swap list = list

isSorted :: List Int -> Bool
isSorted Empty = True
isSorted (NonEmpty x Empty) = True
isSorted (NonEmpty x rest@(NonEmpty y _)) = if x < y then isSorted rest else False

-- pass through the list once, doing the bubbling
singleBubble :: List Int -> List Int
singleBubble Empty = Empty
singleBubble x@(NonEmpty _ Empty) = x
singleBubble (NonEmpty x (NonEmpty y rest)) = if x < y
  then NonEmpty x (singleBubble (NonEmpty y rest))
  else NonEmpty y (singleBubble (NonEmpty x rest))
    -- works, but not ideal: singleBubble (swap (NonEmpty x rest))

bubbleSort :: List Int -> List Int
-- take 2 elements, if correct order, leave alone, otherwise switch
-- take next 2, do the same
-- when end reached, if switches made, do it all again, if not, done
bubbleSort list = if isSorted list then list else bubbleSort (singleBubble list)

data BinaryTree a = Leaf | Branch a (BinaryTree a) (BinaryTree a)