{-# LANGUAGE ScopedTypeVariables #-}
module GraphExamples where

import Data.Maybe (isJust, fromJust, fromMaybe)
import Graphs

instance Eq v => Graph ([v], v -> v -> Bool) v (v, v) where
  vertices (vs, f) = vs

  edges (vs, f) = concat (map (\v0 -> map (\v1 -> (v0, v1)) vs) vs)

  getEdge (vs, f) v0 v1 = if f v0 v1 then Just (v0, v1) else Nothing

  neighbours (vs, f) v = filter (f v) vs

  fromAdjacencyList (vs, edgeList) = (vs, \x y -> fromMaybe False $ ((y `elem`) . map snd) <$> x `lookup` zip vs edgeList)

toAdjacencyList :: forall g v e. Graph g v e => g -> ([v], [[e]])
toAdjacencyList graph = (vertices graph, map getEdgeList (vertices graph))
  where getEdgeList :: v -> [e]
        getEdgeList v0 = map (\v1 -> fromJust (getEdge graph v0 v1)) (neighbours graph v0)

graph :: [[(Int, Int)]]
graph = [
  [(0,8)],
  [(1,0),(1,2),(1,4)],
  [(2,4), (2,5), (2,7)],
  [(3,1)],
  [],
  [],
  [],
  [],
  [(8,4),(8,7)]
  ]

collatz :: Int -> ([Int], Int -> Int -> Bool)
collatz n = ([1..n], \x y -> if even x then y == x `div` 2 else y == 3 * x + 1)

undirected :: forall g v. (Eq v, Graph g v (v, v)) => g -> g
undirected graph = fromAdjacencyList $ toAdjacencyList (vs, \x y -> f x y || f y x)
  where vs :: [v]
        f :: v -> v -> Bool
        (vs, f) = fromAdjacencyList $ toAdjacencyList graph

divisors :: Maybe Int -> ([Int], Int -> Int -> Bool)
divisors x = (vs, \a b -> a `mod` b == 0 || b `mod` a == 0)
  where vs = case x of
               Nothing -> [2..]
               Just n  -> [2..n]